// ==UserScript==
// @name           Left align drupal.org
// @namespace      http://drupal.org/
// @description    Makes drupal.org aligned to the left and issue queue width can expand as much as necessary.
// @include        http://drupal.org/*
// @include        http://api.drupal.org/*
// @include        http://groups.drupal.org/*
// @include        http://association.drupal.org/*
// @include        https://drupal.org/*
// @include        https://api.drupal.org/*
// @include        https://groups.drupal.org/*
// @include        https://association.drupal.org/*
// ==/UserScript==

$ = unsafeWindow.jQuery;

$(".container-12").css('margin-left', '15px');
$(".container-12").css('margin-right', '15px');
$(".container-12").css('width', 'inherit');
$(".container-12").css('min-width', '960px');

$("body.page-project #column-left").removeClass('grid-12');
$("body.page-project #column-left").css('clear', 'both');
$("body.page-project #column-left").css('margin-left', '10px');
$("body.page-project #column-left").css('margin-right', '10px');
